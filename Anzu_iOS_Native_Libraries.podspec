Pod::Spec.new do |s|
    s.name            = "Anzu_iOS_Native_Libraries"
    s.version         = "5.16"
    s.summary         = "Anzu static libraries compiled for iOS"
    s.homepage        = "https://bitbucket.org/AnzuTeam/anzu_sdk_cocoapods_repo"
    s.license         = { :type => 'LGPL', :file => 'LICENSE' }
    s.author          = { "Michael Badichi" => "michael@anzu.io" } # Podspec maintainer
    s.requires_arc    = true
    s.platform        = :ios, "8.0"
    s.source          = { :git => "https://bitbucket.org/AnzuTeam/anzu_sdk_cocoapods_repo.git", :submodules => false }
    s.default_subspec = 'precompiled'
    s.subspec 'precompiled' do |ss|
        ss.vendored_libraries  = 'Libraries/Anzu/SDK/Plugins/iOS/*.a'
        ss.libraries = 'anzu-iOS'
    end
end
